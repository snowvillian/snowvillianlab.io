---
title: Recreating Stickers at the Sign Shop
subtitle: Zodiac Signs
date: 2014-02-20
tags: ["stickers", "signwriting","recreating"]
summary: Recreating stickers with a camera and Adobe Illustrator.
image: /img/posts/signwriting/sticker-eye.jpg
comments: true
---
Most students in New Zealand work after school so they can enjoy their time with friends! My part time job included working at a Parts Department and Sign Writing! Below is an example of the work I did while I was there.

Imagine trying to recreate stickers that are too large to be scanned with your regular printer! Furthermore, ensure that the output is of high resolution to print in large format!

This customer wanted a sticker recreated as soon as possible otherwise it was a four week wait for them to arrive from Holland!

## Here is the original sticker!
![Sticker](/img/posts/signwriting/sticker.jpg)

As you can see the sticker isn't in perfect condition!

![Sticker](/img/posts/signwriting/stickerstitch.png)

The trick was to take photos as close as possible with the camera available at the sign shop and then stitch the images together to get the highest quality image as possible. 

For this I used Photoshop's photo merge feature to ensure the detail in the blue to white gradient was clear enough to copy. 

## My digitally created copy! and final product!
![Sticker](/img/posts/signwriting/sticker2.png)

![Sticker](/img/posts/signwriting/final.jpg)