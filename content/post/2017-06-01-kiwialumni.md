---
title: Kiwi Alumni Group in South Korea
date: 2017-06-01
tags: ["kiwi", "korea", "sony", "newzealand"]
summary: Great way to meet like-minded people in South Korea!
image: /img/posts/kiwialumni/hang-eye.jpg
comments: true
---

Jared is a committee member and the official secretary for the Kiwi Alumni in South Korea. If you are interested in joining, please contact me!


