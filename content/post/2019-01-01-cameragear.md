---
title: My Camera Gear for 2019
date: 2019-01-01
tags: ["camera", "photography", "sony"]
image: /img/posts/cameragear/a6000bg-eye.jpg
summary: Happy New Year Everyone! This year I have decided to create a blog so I can share some of my experience during my time taking photos.
comments: true
---
Happy New Year Everyone! This year I have decided to create a blog so I can share some of my experience during my time taking photos.

### Camera and Lens
#### Sony a6000
I have used this camera for almost 4 years! The quality is still great and still gives other competitor's camera a run for their money. I haven't treated that well, therefore one of the dials at the top of the camera have fallen off! But with a little bit of handy super glue, I was able to stick the dial back on to the camera. 

#### SEL35f18
This is a fantastic lens for video beginners and photographers. The issue with the kit lens provided with the a6000 is that it does not accurately represent the quality of the sensor. Prime lenses provided a much more superior image compared with zoom lenses, including my 70-200mm lens!


#### SEL70200f4
This has been a great addition to my kit, it allows be to take much more emotional pictures at Church service since I can zoom to capture a close up of their face, which shows a lot more emotion than standing back 10 meters with a 35mm lens equipped.

{{< gallery caption-effect="fade" >}}
  {{< figure  link="/img/posts/cameragear/70200lens.jpg" caption="Sony 70-200mm f4.0 Lens"  >}}
  {{< figure  link="/img/posts/cameragear/35lens.jpg" caption="Sony 35mm f1.8 Lens" >}}
  {{< figure  link="/img/posts/cameragear/6000camera.jpg" caption="Sony A600" alt="Sony a6000 camera" >}}
{{< /gallery >}}


### Accessories
#### Zhiyun Crane v2
Since the mass production of hobbyists Gimbals from companies such as Zhiyun and DJI, there has been a lot of content uploaded to Youtube that harness the smooth still video footage that can be produced while using Gimbals. You can now find it available on Aliexpress for less than 400 USD.

#### Godox tt350s
This is a great little flash from China which is compatible with the Sony a6000 for less than 100 USD.

{{< gallery caption-effect="fade" >}}
{{< figure  link="/img/posts/cameragear/zhiyuncrane.jpg" caption="Zhiyun Crane v2" alt="Zhiyun Crane v2" >}}
{{< figure  link="/img/posts/cameragear/tt350flash.jpg" caption="Godox tt350s flash" alt="Godox tt350s flash" >}}
{{< /gallery >}}

  
  
