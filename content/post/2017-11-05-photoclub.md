---
title: Finder Photography Club in Korea
date: 2017-11-05
tags: ["finder", "life", "education", "photography"]
summary: Taking photos with the locals!
image: /img/fallkorea-eye.jpg
comments: true
---
Some of the most talented bunch of photographers I have ever met! 

<script src="https://cdn.jsdelivr.net/npm/publicalbum@latest/dist/pa-embed-player.min.js" async></script>
<div class="pa-embed-player" style="width:100%; height:480px; display:none;"
  data-link="https://photos.app.goo.gl/GVZdn8wCxmOo7ZmM2"
  data-title="파인더 단풍촬영 2017"
  data-description="19 new photos added to shared album">
  <img data-src="https://lh3.googleusercontent.com/uSWGgPmiJZlPU5CNiS3ejNnAThl8l8yVeoYbThTDDFK7WX1wBc3Qn7jQ834UHSfVQrHqq3ReMTH7xCsQotNhi84raVcYW06daICSsjHqeD1NDQVhCp61JHLrfZUjleaN_WyFvmDM2A=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/OhwJJd6D9QsU33NIDHA_44xx7Vbx0b7Gwwh3Q92diUSC4IO3pAjfV90zLmyus6LQ60Za2FQvRb0ghDW7PwuvCBP8EprKORnnhdLkfWDg-RoyEKw6sipni0LF19NRi7zfqnImcU7CkQ=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/XoA5PqEOi9tS7ULKaEF_7j-lZlPV2WCcdiK4NLkg4Z2MftjppPlWjF0ay8soPSr3EaApwPZib5kBveYihcnLWTrbEpeOKPCoHkeKgpuEln1rQu80dMyuByi_s3UqoIrjzJ8S20XLwg=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/HkzeDqJk4g3MCySeXpE-YA_Ck3BasA43oGXWcP2w66tUo-71XeAC7qGL5SNBkaS-0HgyQVFbuLeac5ooNoZoj5S0EG8zxq4Qov3ZT5Nzt0MHPRvbtA824Z9ZiPtVAdnIvaYohph79A=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/wj8XmXao4Kw5jSqru6ho83YeTUtEveCkl7tsDIjlnxsPaa_oSV9x9cIzmN_Z0uOjvwbx2wWUtPyDFRsqTG8LUhkl3ObScJ0r91lgCK1p5OqzKLyaVi_zwpFP3JtLk9vCpsXZQK4Wvw=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/nj09v7ALOl5-mVZ3sFED-0Uh8dwQU1awFPQEWSrf6TFnxsDTbG0d5rBtaxmxdmVmezXsJsxNCExIE4r18XZGcBxKFNW7ijNf7laam0NGFacJhLlMl02T9UK1ILGt5EKhoL7jkwvhqA=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/TKGRXC5ey3TKob5w2j4aR7KGYDXhGePhNqNc9gwUZBG1bqx7-Lr7Hu9m8IwEXtV9_NriNhDU52r67zxr8YKdzL6jGSBy8llkRE1gKmLrl9sMzLinGw5ClwWTB4EinyC7Da-k0Gd3cQ=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/PERV8LVU7nBbcP_P_sL6CZyO7GlI389Kz5E2WsEDBuBypa0MjkLIN84JCYBqX598AhWcYyMBcg0iALf5zvLeXNFbEgbNkFjpaWEa4eOX0fDfeEJ8JU_f3ht5ESRI4Ey-EEGK96sZdA=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/O5gpqCAUlPfYzYu4V8voafZ4JgV0-o6Du2jipM6oC5kLMHbPMH7qC1ZcJtA2u-Bq9CY4_7eBw7LRKtAE4lVPGnRHqeAGuDmUGt_IeLYIJEdkkDNZa6vJa_e8onZZ5WKJoVjEDU-YSw=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/_0h5tuaYrNDRZaDnV0CuZEjTmhL4yzPNS-btOJA9q7Vq3SK7OR50aNhLIlexrK7imhusELWnOuy3E6QOiQeImbndoVgpgpHwFnXQpIW6U32_aMp5YSHbuUc2GWGUUA9MTt1SLJ2V7A=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/I2DIEoD0MXeNkrI6hSb7b1xDqDJFofJjpwS78cdU4ejfnQrB4h61kwMnh6FnJzKJyr2-BpPmjJGsRlXOm2W-xiF3HG0uXFTMlvBxY-9KTKPOHMHNbluYuqlXwpD-3kMU6cYMiuaybA=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/arj-cBhn-ZDTT3JqRkEjml7OjndR4ZMKmWP8c5C8kgQM65SLYlYqAidSMFAN_jD3xxWBrHaQrLgDuCPfJ8icFS6aGBnAuPAhHJwItBTv70K2nUfnl5vaj7Wjaj_dLIY0PtFAO75uqw=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/_-7KB0rRLxjKqBcuy-iybAo_GUustOTs-GlhwdFKVlfMgB2ZQKsFFfayqBSgDtlZHy-AZLu3ol4dYN4I70ie7lcosZ9eI70TJzy20U1grcnCAOjtIBAzVSJ9TauXEa73p7ZSTeFLCg=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/bqJ_WPWFkH2d05vlZNWORaoCBq5z5uU5MwKon-T-eIjNHNeV4yD1imdN2EMB0zkZdfQ39UPprF2cV0yAJf6sMGm6lSwHhdtB8uG9tafmM0zn1fYVBySMRlbIDEibuRf2Oes2akQShg=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/KbBieAFBIicl8m6U61yEcuLOmObiBkVzSdWMZwFwLmke0K76Y2My8pdVBDke9GNi30UgVoqGKnrUAH33OKoqBBHDmQauP6O8OEVEUBz7z44HJVZXnjPmS6jyr3kli9tQSwXLZoDQTA=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/qp0zJjHoT0X17aWwTZrhgF9LfU7r-mLfnuNjCrgtmmmD-nDvV4xmgk968uOOH_dWtxP0SZ3RJQvG85ERaVXhhnX3Rk-bRqZq-dxttyxxDzDmKFD-i2L_fnvp881ETQXn04KNhC0ESA=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/e30iox7aZ9gaQh0yRu63WabQQfOQiJTSrVDyCkrsFNVFvJFBnj2PAG3bEi1rOwhekToAPtQ3JqCHSwE2Mnox41kmwbuCN4gojlJ5rucJMe7MUoSgEveoP59VI-2-LZzxgGfCNk9DZQ=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/d9zZq27bCHj0dpMfmGjkiYYc6JKwvGo32EbVyFSzCjBt3ZH-7iOhlgjdnuCIkn7hnqGPzgFGRKsxUsf_n95S9y_FUMMmbLa9x5UAB4VZYHFIQmAl1y9h7VZ7wr2fxOHsC5qt0vFGWw=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/wx5Vfza9lj78Ms9Dct-JZLDXcczq-976fKfVZnPriWEyECp6a33AU_Rq3UPrGIQVhJBae1TfppQGEK7I-a5QNdACMOOHPoR6sQwWK5DFGpkBLFBRRXVr3RBhRfeQ0N12Jq8FluNUbg=w1920-h1080" src="" alt="" />
</div>
