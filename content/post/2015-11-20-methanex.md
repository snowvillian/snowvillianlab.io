---
title: Working at Methanex New Plymouth NZ
subtitle: Internship at one of the world's largest methonal producers
date: 2015-11-20
tags: ["internship", "work","graduate"]
summary: My story about working in the Information Technology Department for Methanex NZ.
image: /img/posts/newplymouth/factory-eye.jpg
bigimg: [{src: "../../img/posts/newplymouth/pano-eye.jpg", desc: "At the top of Paratutu Rock"}]
comments: true
---

I spent three months at Methanex in the Information Technology Department. Unfortunately I am unable to disclose any of the results.

If you have any questions, please contact me via email.


![../../img/posts/newplymouth/pano.jpg](../../img/posts/newplymouth/slideone.png)

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10430.380630227935!2d174.27959726181035!3d-38.99316473660163!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc38de2ecb85c594c!2z66mU64uk64Sl7IqkIOuJtOyniOuenOuTnCDroZztirg!5e0!3m2!1sko!2skr!4v1547365890709" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>