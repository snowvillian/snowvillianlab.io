---
title: Cycling in Korea
date: 2018-08-11
tags: ["korea", "cycling", "strava", "bike", "sport"]
bigimg: [{src: "../../img/posts/cycle/cyclebg.jpg", desc: "Cycling in Korea!"}]
summary: Cycling in South Korea
image: /img/posts/cycle/cyclebg-eye.jpg
comments: true
---

Here is one of my favourite routes in Korea.

<iframe height='405' width='100%' frameborder='0' allowtransparency='true' scrolling='no' src='https://www.strava.com/activities/1762751697/embed/070e2eca1cdf396129be0a74469da18451ec8936'></iframe>