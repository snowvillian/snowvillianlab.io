---
title: Trip to China
date: 2018-05-01
bigimg: [{src: "../../img/greatwall.jpg", desc: "Livin' in up in China"}]
tags: ["travel", "china", "life"]
summary: I visited China for the first time!
image: /img/greatwall-eye.jpg
comments: true
---

I visited Beijing, China for a total of five days!

Flying from Gimpo to Beijing is quite convenient coming from Suwon. It is possible to take the Bundang Line and then transfer in Sinnonhyeon!

First day in China, Haegum and I visited Beijing University, Haegum's University! Security is tight and only students and foreigners can enter the campus.


{{< instagram BmxHrAXAtNk >}}