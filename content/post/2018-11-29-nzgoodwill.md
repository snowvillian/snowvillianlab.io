---
title: New Zealand's Goodwill
date: 2018-11-29
tags: ["goodwill", "kiwi", "events", "korea"]
comments: true
image: /img/posts/kiwicham/scholarship-eye.jpg
summary: How New Zealand is contributing to Korean Society.
---

New Zealand Ambassador to Korea Philip Turner and Chris Raciti, CEO ANZ Korea, present New Zealand Study Scholarship to two students from Songjukwon Girl’s Residence at the 2018 The Kiwi Chamber Year End Grand Hui. Over 250 guests attended the year-end "All Kiwi experience."
